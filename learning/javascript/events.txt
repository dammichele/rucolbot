Event 		Occurs
onabort 	When an image’s loading is stopped before completion
onblur 		When an element loses focus
onchange 	When any part of a form has changed
onclick 	When an object is clicked
ondblclick 	When an object is double-clicked
onerror 	When a JavaScript error is encountered
onfocus 	When an element gets focus
onkeydown 	When a key is being pressed (including Shift, Alt, Ctrl, and Esc)
onkeypress 	When a key is being pressed (not including Shift, Alt, Ctrl, and Esc)
onkeyup 	When a key is released
onload 		When an object has loaded
onmousedown 	When the mouse button is pressed over an element
onmousemove 	When the mouse is moved over an element
onmouseout	When the mouse leaves an element
onmouseover 	When the mouse passes over an element from outside it
onmouseup 	When the mouse button is released
onsubmit 	When a form is submitted
onreset 	When a form is reset
onresize 	When the browser is resized
onscroll 	When the document is scrolled
onselect 	When some text is selected
onunload 	When a document is removed
