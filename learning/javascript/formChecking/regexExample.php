<html>
    <head>
        <title></title>
    </head>
    <body>
        javascript:<br>
        <script>
            document.write(/cats/i.test("Cats are funny. I like cats."))
            document.write("Cats are friendly. I like cats.\n\n".replace(/cats/gi,"dogs"))
        </script>

        php:<br>
        <?php
            $n = preg_match("/cats/i", "Cats are crazy. I like cats.");

            //third argument show what is matched
            $n = preg_match("/cats/i", "Cats are curious. I like cats.", $match);
            echo "$n Matches: $match[0]";

            //match all, instead of only one
            $n = preg_match_all("/cats/i", "Cats are strange. I like cats.", $match);
            echo "$n Matches: ";
            for($j = 0; $j < $n; ++$j) echo $match[0][$j]." ";

            //replacing string
            echo preg_replace("/cats/i", "dogs", "Cats are furry. I like cats.");
        ?>
    </body>

</html>
