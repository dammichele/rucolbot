<?php
echo <<<_END
<html>
    <head>
        <title>
            PHP form Upload
        </title>
    </head>
    <body>
        <form method="post" action="uploadFile.php" enctype="multipart/form-data">
            Select File: <input type="file" name="filename" size="10"/>
            <input type="submit" value="Upload"/>
        </form>
_END;

    /* NO controlli
        if($_FILES) {
            $name = $_FILES['filename']['name'];
            move_uploaded_file($_FILES['filename']['tmp_name'],$name);
            echo "Uploaded image '$name' <br /><img src='$name'/>";
        }
    */

    //SI controlli
    if($_FILES) {
        $name = $_FILES['filename']['name'];

        switch($_FILES['filename']['type']) {
            case 'image/jpeg':
                $ext = 'jpg';
                break;
            case 'image/gif':
                $ext = 'gif';
                break;
            case 'image/png':
                $ext = 'png';
                break;
            case 'image/tiff':
                $ext = 'tiff';
                break;
            default:
                $ext = '';
                break;
        }
        if($ext) {
           // $n = "image.$ext";
            $name = strtolower(preg_replace("/[^A-Za-z0-9.]/", "", $name));//oppure purifico nome e lo rendo minuscolo per compatibilità con sistemi case sensitive o meno
            move_uploaded_file($_FILES['filename']['tmp_name'], $n);
            echo "Uploaded image '$name' as '$n':<br />";
            echo "<img src='$n' \>";
        } else echo "'$name' is not an accepted image file";
    } else echo "No image has been uploaded";
echo "</body></html>"
?>
