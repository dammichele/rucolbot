<?php
//se consento passaggio di ID session su querystring posso effettuare questo controllo
    session_start();
    //imposto path salvataggio session
    ini_set('session.save_path', '/home/user/myaccount/session');

//potrei anche forzare l'utilizzo dei cookie e far ignorare il PHPSESSID=
    //ini_set('session.use_only_cookies', 1);

    if (!isset($_SESSION['initiated'])) {
        session_regenerate_id();
        $_SESSION['initiated'] = 1;
    }

    if (!isset($_SESSION['count'])) $_SESSION['count'] = 0;
    else ++$_SESSION['count'];

    echo $_SESSION['count'];
?>

