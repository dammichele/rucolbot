<?php
    //$var = sanitizeString($_POST['user_input']);
    //oppure
    //$var = sanitizeMySQL($connection, $_POST['user_input'];     

    function sanitizeString($var)
    {
        $var = stripslashes($var);//toglie escape vari
        $var = strip_tags($var);//toglie i tag
        $var = htmlentities($var);//trasforma caratteri tag in medesimi escapati
        return $var;
    }

    function sanitizeMySQL($connection, $var)
    {
        $var = $connection->real_escape_string($var);//escape caratteri SQL
        $var = sanitizeString($var);
        return $var;
    }
?>